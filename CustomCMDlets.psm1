function Get-StorageHbaInfo ($ClusterName) {
	$myreport = @()
	foreach ($cluster in get-cluster $ClusterName){
		$hosts = $cluster | get-vmhost 
		foreach ( $vmhost in $hosts ) {
		write-host "Collect info for ESXi host : " $vmhost
			$hbas = Get-VMHost $vmhost | Get-VMHostHba  
			$path = $hbas | Get-ScsiLun | Get-ScsiLunPath
			foreach ( $hba in $hbas ) {
				$node = "{0:x}" -f $hba.NodeWorldWideName
				$port = "{0:x}" -f $hba.PortWorldWideName
				#$path = $hba | Get-ScsiLun | Get-ScsiLunPath
				$active = ($path | where { $_.State -eq "Active" -and $_.Name -like "$($hba.name)*"}).count
				$dead = ($path | where { $_.State -eq "Dead" -and $_.Name -like "$($hba.name)*"}).count
				$standby = ($path | where { $_.State -eq "Standby" -and $_.Name -like "$($hba.name)*"}).count
				$myreport += new-object psobject -property @{
							clustername  = [string]$cluster.name
							hostname     = [string]$vmhost
							hba			 = [string]$hba.name
							node 		 = [string]$node
							port 		 = [string]$port
							active		 = $active
							dead		 = $dead
							standby      = $standby
				}
			}
		}
	}
	$myreport | select clustername, hostname, hba, node, port, active, dead, standby | sort hostname | format-table  -autosize -wrap
}

function Get-StorageLunInfo ($HostName){
	$count=0
	$myreport = @()
	Get-VMHost -Name $HostName | Get-Datastore |	Where-Object {$_.ExtensionData.Info.GetType().Name -eq "VmfsDatastoreInfo"} |
	ForEach-Object {
		$count++
		if ($_)
		{
			$Datastore = $_
			$CanonicalName = $Datastore.ExtensionData.Info.Vmfs.Extent.diskname 
			$LunInfo	 = get-vmhost -name $HostName | get-scsilun $CanonicalName 
			$path		 = $LunInfo |  Get-ScsiLunPath | Group-Object -Property state
			$multipath	 = $LunInfo.MultipathPolicy
			$vendor		 = $LunInfo.Vendor
			$cap		 = $LunInfo.CapacityGB
			$myreport += new-object psobject -property @{
				id			 = $count
				datastore 	 = $Datastore.Name
				Lun		  	 = $CanonicalName 
				active		 = ($path | where { $_.Name -eq "Active"}).count
				dead		 = ($path | where { $_.Name -eq "Dead"}).count
				standby      = ($path | where { $_.Name -eq "Standby"}).count
				MultiPath	 = $Multipath
				Vendor		 = $vendor
				CapacityGb	 = $cap
			}
		}
	}
	$myreport | select id, vendor, lun, datastore, active, standby, dead, multipath, capacitygb| format-table -autosize -wrap
}

function Get-StorageUnallocated ($ClusterName){
	$myreport = @()
	foreach ($Cluster in get-cluster $ClusterName){
		$VMhosts = $Cluster | get-vmhost
		foreach ($HostName in $VMhosts){
			$Storage = Get-View $HostName.ExtensionData.ConfigManager.DatastoreSystem
			foreach ($Lun in $Storage.QueryAvailableDisksForVmfs($null)){
				$myreport += new-object psobject -property @{
								clustername  = [string]$cluster.name
								hostname     = [string]$HostName
								lunID        = $Lun.canonicalName
				}
			}
		}
	}
	$myreport | format-table -autosize -wrap
}

function Get-Capacity ($ClusterName){
	$myreport = @()
	
	$ViewCluster = get-cluster $CLusterName | get-view
	$ObjVM = get-vm -location $ClusterName
	$NrOfHosts = $ViewCluster.Summary.NumHosts
	
	$OnVM = ($ObjVM | where {$_.PowerState -eq "PoweredOn"} | Measure-Object).Count
	$OnallocatedCPU = ($ObjVM | where{$_.PowerState -eq "PoweredOn"} | measure -InputObject {$_.NumCpu} -Sum).Sum
	$OnallocatedRAM = ($ObjVM | where{$_.PowerState -eq "PoweredOn"} | measure -InputObject {$_.MemoryMB} -Sum).Sum
	$OffVM = ($ObjVM | where {$_.PowerState -eq "PoweredOff"} | Measure-Object).Count
	$OffalocatedCPU = ($ObjVM | where{$_.PowerState -eq "PoweredOff"} | measure -InputObject {$_.NumCpu} -Sum).Sum
	$OffallocatedRAM = ($ObjVM | where{$_.PowerState -eq "PoweredOff"} | measure -InputObject {$_.MemoryMB} -Sum).Sum
	
	$TotalClusterRam = [Math]::Round($ViewCluster.Summary.TotalMemory/1MB)
	$TotalCoresCpu = $ViewCluster.Summary.NumCpuCores
	$TotalLogicalCpu = $ViewCluster.Summary.NumCpuThreads
	$RatiovCpupCpu = [Math]::Round($OnallocatedCPU/$TotalCoresCpu, 2)
	
	$myreport += new-object psobject -property @{
				Cluster		 = $ClusterName
				CpuCores 	 = $TotalCoresCpu
				CpuLogical 	 = $TotalLogicalCpu 
				AllocatedCPU = $OnallocatedCPU
				VtoPCPU		 = $RatiovCpupCpu
				OffAllocatedCPU = $OffalocatedCPU
				TotalRam	 = $TotalClusterRam
				AllocatedRAM = $OnallocatedRAM
				OffallocatedRAM = $OffallocatedRAM
			}
	
	$myreport | select Cluster,CpuCores,CpuLogical,AllocatedCPU,VtoPCPU,TotalRam,AllocatedRAM,OffAllocatedCPU,OffallocatedRAM
	
	$myhostreport = @()
	foreach ($vmhost in get-cluster $CLusterName | get-vmhost){
		$ObjVM = get-vmhost $vmhost | get-vm
		$OnVM = ($ObjVM | where {$_.PowerState -eq "PoweredOn"} | Measure-Object).Count
		
		$OnallocatedCPU = ($ObjVM | where{$_.PowerState -eq "PoweredOn"} | measure -InputObject {$_.NumCpu} -Sum).Sum
		$OnallocatedRAM = ($ObjVM | where{$_.PowerState -eq "PoweredOn"} | measure -InputObject {$_.MemoryMB} -Sum).Sum
		$TotalHostRam = $vmhost.MemoryTotalGB
		[int]$TotalHostpCpu = $vmhost.numCpu
		
		$TotalHostLCpu = [Math]::Round($vmhost.numCpu * 2)
		$RatiovCpupCpu = [Math]::Round($OnallocatedCPU/$TotalHostpCpu, 2)
		
		$myhostreport += new-object psobject -property @{
			Host		 = $vmhost.name
			CpuCores 	 = $TotalHostpCpu
			CpuLogical 	 = $TotalHostLCpu 
			AllocatedCPU = $OnallocatedCPU
			VtoPCPU		 = $RatiovCpupCpu
			TotalRam	 = $TotalHostRam
			AllocatedRAM = $OnallocatedRAM
		}
		
	}
	
	$myhostreport | select host,cpucores,cpulogical,allocatedcpu,VtoPCPU,TotalRam,AllocatedRAM | Out-GridView
}




function New-DistributedPortGoup ($DatacenterName, $ClusterName , $VlanID , $VlanDescription){
##this is a specific function that will generate a specific kind of portgroup based on DC , CL, VLAN ID
##eg DC=TEST_DATACENTER , CL=CLUSTER_TEST , VLANID = 20 ==>> 0020-TEST_DATACENTER-CLUSTER_TEST-VLAN_DESCRIPTION
	if (!$DatacenterName) {
		write-host "No Datacenter Name specified usage of command New-DistributedPortGoup 'DatacenterName' 'CLusterName' 'VLANID' 'VlanDescription'" 
		break
	}
	if (!$ClusterName) {
		write-host "No Cluster Name specified usage of command New-DistributedPortGoup 'DatacenterName' 'CLusterName' 'VLANID' 'VlanDescription'" 
		break
	}
	if (!$VLANID) {
		write-host "No VLANID specified usage of command New-DistributedPortGoup 'DatacenterName' 'CLusterName' 'VLANID' 'VlanDescription'" 
		break
	} else {
			if (!($VlanID -match '^[0-9]+$')) {
				write-host "The VLanID is not a number" 
				break
			}
	}
	if (!$VlanDescription) {$VlanDescription = "NoDescription"}
	[string]$VlanID = $VlanID
	$VirtualDS = get-vdswitch -vmhost(get-vmhost -location $ClusterName)
	
	if ($VlanID.length -eq 1) { $VirtualDPG = "000" + $VlanID + "-" }
	if ($VLanID.length -eq 2) { $VirtualDPG = "00" + $VlanID + "-" }
	if ($VLanID.length -eq 3) { $VirtualDPG = "0" + $VlanID + "-" }
	if ($VLanID.length -eq 4) { $VirtualDPG = $VlanID + "-" }
	
	$VirtualDPG = $VirtualDPG + $DatacenterName + "-" + $ClusterName + "-" + $VlanDescription
	$Uplink1 = (Get-VDPortgroup -name '*vmotion*' -VDSwitch $VirtualDS.name).extensiondata.config.defaultportconfig.uplinkteamingpolicy.uplinkportorder.standbyuplinkport
	$Uplink2 = (Get-VDPortgroup -name '*vmotion*' -VDSwitch $VirtualDS.name).extensiondata.config.defaultportconfig.uplinkteamingpolicy.uplinkportorder.activeuplinkport
	
	write-host "=====New Virtual Distributed Port Group====="
	write-host "[DC              ] " $DatacenterName
	write-host "[CL              ] " $ClusterName
	write-host "[VDS             ] " $VirtualDS.name
	write-host "[VLAN ID         ] " $VLANID
	write-host "[VLAN DESCRIPTION] " $VlanDescription
	write-host "[VPG  NAME       ] " $VirtualDPG
	write-host "[ACTIVE   UPLINK ] " $uplink1
	write-host "[STANDBEY UPLINK ] " $uplink2
 	
	##crate the distributed virtual port group
	get-vdswitch -name $VirtualDS.name | new-vdportgroup -Name $VirtualDPG -vlanid $VLanID
	##set the nicteaming failover order 
	get-vdswitch -name $VirtualDS.name | get-vdportgroup -Name $VirtualDPG | get-VDUplinkTeamingPolicy | set-VDUplinkTeamingPolicy -activeuplinkport $Uplink1 -standbyuplinkport $Uplink2
}

function check-dvs($ClusterName){
##this is a specific function for my environments
	if (!$ClusterName) {
		write-host "No Cluster Name specified usage of command Check-DVS 'CLusterName'" 
		break
	}
	
	$outfile = "Check-DVPG "+$ClusterName+".txt"
	
	$ClusterView = get-view -viewtype ClusterComputeResource | where {$_.name -eq $ClusterName}
	
	#find the DC under which the cluster resides
	$DatacenterView = get-view -viewtype Datacenter | where {$_.HostFolder -eq $ClusterView.Parent}
	
	#take all the hosts under the cluster
	$HostView = get-view -viewtype HostSystem -searchRoot $ClusterView.Moref
	
	#take all the DVS
	$DistributedSwitchView = get-view -viewtype DistributedVirtualSwitch
	
	#find the dvs connected
	$DVSPattern = "DVS-"+ $DatacenterView.Name + "-" + $ClusterName
	  
	$DVSUUID = $HostView.config.network.vnic.spec.distributedvirtualport.switchuuid | select -uniq 
	$NrofSw = $DVSUUID | measure
	
	if ( $NrofSw.count -gt 1) {
		write-host $ClusterName " has too many DVS configured "
			foreach ($DVS in $DVSUUID){
				$DVSName = ($DistributedSwitchView | where {$_.uuid -eq $DVS}).Name
				write-host $DVSName
			}
		break
	} 
	elseif ($NrofSw.count -lt 1) {
		write-host $ClusterName " has no DVS configured "
		break
	} 
	
	$DVSName = ($DistributedSwitchView | where {$_.uuid -eq $DVSUUID}).Name
	write-host $ClusterName " is connectect to Distributed Virtual Switch " $DVSName
	#found the dvs
	
	#check if the DVS Name is according to the standard
	#DVS-DCNAME-CLUSTERNAME
	if ($DVSName -eq $DVSPattern){
		write-host "DVS name is correct " -foreground green
	} else {
		write-host "DVS name is incorrect the correct name should be $DVSPattern " -foreground red
		write-host "Set-VDSwitch -VDSwitch '$DVSName' -Name '$DVSPattern'" -foreground red
	}
	#done with the check
	
	#DVS Moref
	$DVSMoref = ($DistributedSwitchView | where {$_.uuid -eq $DVSUUID}).Moref
	
	#take all the dvpg
	$DistributedPortGroupView = get-view -viewtype DistributedVirtualPortgroup | where {$_.config.distributedvirtualswitch -eq $DVSMoref}
	
	#identify the vmotion/mgmtn portgroups
	#check the naming convention & and change it
	#check active/stanby UL & and change them
	$ManagementPGKey = $HostView.config.network.vnic.spec.distributedvirtualport.portgroupkey | select -uniq
	$NrofManagementPG = $ManagementPGKey | measure
	if($NrofManagementPG.count -eq 2){
		#check if the naming convention is per NNIT Standard if Not will generate the commands to change it
		foreach ($ManagementTraffic in $ManagementPGKey  ){
			$ManagementTrafficName = ($DistributedPortGroupView | where {$_.key -eq $ManagementTraffic}).Name
			[string]$ManagementTrafficVLAN = ($DistributedPortGroupView | where {$_.key -eq $ManagementTraffic}).config.DefaultPortConfig.vlan.vlanID
		
			#set the pattern for the vlanID .. it will be used to check the naming convention
			if ([string]$ManagementTrafficVLAN.length -eq 1) {$ManagementTrafficVLAN = "000" + $ManagementTrafficVLAN }
			if ([string]$ManagementTrafficVLAN.length -eq 2) {$ManagementTrafficVLAN = "00" + $ManagementTrafficVLAN }
			if ([string]$ManagementTrafficVLAN.length -eq 3) {$ManagementTrafficVLAN = "0" + $ManagementTrafficVLAN }
			if ([string]$ManagementTrafficVLAN.length -eq 4) {$ManagementTrafficVLAN = $ManagementTrafficVLAN }
			
			$ManagementTrafficPatternMGMTN   = "MGMTN-"+$ManagementTrafficVLAN+"-"+$DatacenterView.Name+"-"+$ClusterName
			$ManagementTrafficPatternVMOTION = "VMOTION-"+$ManagementTrafficVLAN+"-"+$DatacenterView.Name+"-"+$ClusterName 
			
			$Prefix = $ManagementTrafficName.split("-",[System.StringSplitOptions]::RemoveEmptyEntries)
			
			if ($ManagementTrafficName -like "$ManagementTrafficPatternMGMTN*" -or $ManagementTrafficName -like "$ManagementTrafficPatternVMOTION*"){
				if ($Prefix[0] -eq "MGMTN") {
					$ManagementTrafficMGMTNName = $ManagementTrafficName
					write-host "Management network "$ManagementTraffic $ManagementTrafficName " Name is Correct " -foreground green
				}
				if ($Prefix[0] -eq "VMOTION") {
					$ManagementTrafficVMOTIONName = $ManagementTrafficName
					write-host "VMOTION network " $ManagementTraffic $ManagementTrafficName "  Name is Correct " -foreground green
				}
			} else {
				write-host $ManagementTraffic $ManagementTrafficName " the Name is inCorrect " -foreground red
				
				if ($Prefix[0] -eq "MGMTN"){
					write-host "Get-VDPortgroup -Name '$ManagementTrafficName' | Set-VDPortgroup -Name '$ManagementTrafficPatternMGMTN'" -foreground red 
				}
				if ($Prefix[0] -eq "VMOTION"){
					write-host "Get-VDPortgroup -Name '$ManagementTrafficName' | Set-VDPortgroup -Name '$ManagementTrafficPatternVMOTION'" -foreground red
				}
				
				$error = 1
			}
		}
		#done with the naming convention

		#check Active Stanby UL
		#if the mgmtn and vmotion don't have the correct naming convention break
		if (!($error -eq 1)){
			$ManagementTrafficMGMTN = get-view -viewtype DistributedVirtualPortgroup -Filter @{"Name"="$ManagementTrafficMGMTNName"}
			$ManagementTrafficVMOTION = get-view -viewtype DistributedVirtualPortgroup -Filter @{"Name"="$ManagementTrafficVMOTIONName"}
			$ManagementTrafficMGMTNActiveUL = $ManagementTrafficMGMTN.config.defaultportconfig.UplinkTeamingPolicy.uplinkportorder.activeuplinkport
			$ManagementTrafficVMOTIONActiveUL = $ManagementTrafficVMOTION.config.defaultportconfig.UplinkTeamingPolicy.uplinkportorder.activeuplinkport
			$ManagementTrafficMGMTNStandbyUL = $ManagementTrafficMGMTN.config.defaultportconfig.UplinkTeamingPolicy.uplinkportorder.standbyuplinkport
			$ManagementTrafficVMOTIONStandbyUL = $ManagementTrafficVMOTION.config.defaultportconfig.UplinkTeamingPolicy.uplinkportorder.standbyuplinkport
			
			if (($ManagementTrafficMGMTNActiveUL | measure).count -eq ($ManagementTrafficMGMTNStandbyUL| measure).count -or ($ManagementTrafficVMOTIONActiveUL | measure).count -eq ($ManagementTrafficVMOTIONStandbyUL| measure).count){
				if ($ManagementTrafficMGMTNActiveUL -like "*2"){
					write-host "MGMTN / VMOTION active UL are set to the 2nd UL " $ManagementTrafficMGMTNActiveUL $ManagementTrafficVMOTIONActiveUL -foreground green
				} else {
					write-host "MGMTN / VMOTION active UL are not set to the 2nd UL " $ManagementTrafficMGMTNActiveUL $ManagementTrafficVMOTIONActiveUL -foreground red
					write-host "get-vdswitch -name $DVSName | get-vdportgroup -Name $ManagementTrafficMGMTN | get-VDUplinkTeamingPolicy | set-VDUplinkTeamingPolicy -activeuplinkport $ManagementTrafficMGMTNStandbyUL -standbyuplinkport $ManagementTrafficMGMTNActiveUL" -foreground red
					write-host "get-vdswitch -name $DVSName | get-vdportgroup -Name $ManagementTrafficVMOTION | get-VDUplinkTeamingPolicy | set-VDUplinkTeamingPolicy -activeuplinkport $ManagementTrafficMGMTNStandbyUL -standbyuplinkport $ManagementTrafficMGMTNActiveUL" -foreground red
					break
				}
			} else {
				write-host "There is a difference between ammount of active stanby UL " -foreground red
				write-host "MGMTN Active UL " $ManagementTrafficMGMTNActiveUL -foreground red
				write-host "MGMTN Stanbdy UL " $ManagementTrafficMGMTNStandbyUL -foreground red
				write-host "VMOTION Active UL " $ManagementTrafficVMOTIONActiveUL -foreground red
				write-host "VMOTION Stanbdy UL " $ManagementTrafficVMOTIONStandbyUL -foreground red
				write-host "get-vdswitch -name $DVSName| get-vdportgroup -Name $ManagementTrafficMGMTN | get-VDUplinkTeamingPolicy | set-VDUplinkTeamingPolicy -activeuplinkport $ManagementTrafficMGMTNStandbyUL -standbyuplinkport $ManagementTrafficMGMTNActiveUL" -foreground red
				write-host "get-vdswitch -name $DVSName | get-vdportgroup -Name $ManagementTrafficVMOTION | get-VDUplinkTeamingPolicy | set-VDUplinkTeamingPolicy -activeuplinkport $ManagementTrafficMGMTNStandbyUL -standbyuplinkport $ManagementTrafficMGMTNActiveUL" -foreground red
				break
			}
		} else {
				write-host "First fix the namings of the MGMTN and VMOTION networks" -foreground red
				break
		}
		#done
	} else {
		write-host "The ammount of Management Traffic PG is more / less then 2"
		foreach ($ManagementTraffic in $ManagementPGKey  ){
			$ManagementTrafficName = ($DistributedPortGroupView | where {$_.key -eq $ManagementTraffic}).Name
			write-host $ManagementTraffic $ManagementTrafficName  -foreground red
		}
		break
	}
	#done with the management portgroups
	
	#check the vm network portgroup and ignore the mgmtn/vmotion and dvuplinks portgroups
	foreach ($DVPGInfo in $DistributedPortGroupView | where {$_.name -notlike "$ManagementTrafficMGMTNName*" -and $_.name -notlike "$ManagementTrafficVMOTIONName*" -and $_.name -notlike "*dvuplinks*"}){
		#check if the active/stanby UL are correct
		write-host "check " $DVPGInfo.name
		$DVPGActiveUL = $DVPGInfo.config.defaultportconfig.UplinkTeamingPolicy.uplinkportorder.activeuplinkport
		$DVPGStanbdyUL = $DVPGInfo.config.defaultportconfig.UplinkTeamingPolicy.uplinkportorder.standbyuplinkport
		if (($DVPGActiveUL | measure).count -eq ($DVPGStanbdyUL | measure).count){
			write-host $DVPGInfo.name " has the same ammount of active / standby UL" -foreground green
		} else {
			write-host $DVPGInfo.name " has different ammount of active / standby UL" -foreground red
			write-host $DVPGInfo.name ($DVPGActiveUL | measure).count " Active UL : " $DVPGActiveUL " vs " ($DVPGStanbdyUL | measure).count " Stanbdy UL " $DVPGStanbdyUL -foreground red 
			
			write-output "get-vdportgroup -Name '$($DVPGInfo.name)' | Get-VDUplinkTeamingPolicy  | Set-VDUplinkTeamingPolicy -StandbyUplinkPort $ManagementTrafficVMOTIONActiveUL -ActiveUplinkPort $ManagementTrafficVMOTIONStandbyUL" >> $outfile
		} 
		
		if ($DVPGActiveUL -eq $ManagementTrafficVMOTIONStandbyUL){
			write-host $DVPGInfo.name " has the correct UL set as active/standby " $DVPGActiveUL $DVPGStanbdyUL -foreground green
		} else {
			write-host $DVPGInfo.name " has the same active/stanby UL as the Management Networks"   -foreground red
			write-output "get-vdportgroup -Name '$($DVPGInfo.name)' | Get-VDUplinkTeamingPolicy  | Set-VDUplinkTeamingPolicy -StandbyUplinkPort $ManagementTrafficVMOTIONActiveUL -ActiveUplinkPort $ManagementTrafficVMOTIONStandbyUL" >> $outfile
		}
		
		#done 
		
		#check if the name of the  dvpg is according to nnit standard
		$DVPGInfoDescription = $DVPGInfo.name.split("-",[System.StringSplitOptions]::RemoveEmptyEntries)
		[string] $DVPGInfoVLAN = $DVPGInfo.config.DefaultPortConfig.vlan.vlanID 
		if ([string]$DVPGInfoVLAN.length -eq 1) {$DVPGInfoVLAN = "000" + $DVPGInfoVLAN }
		if ([string]$DVPGInfoVLAN.length -eq 2) {$DVPGInfoVLAN = "00" + $DVPGInfoVLAN }
		if ([string]$DVPGInfoVLAN.length -eq 3) {$DVPGInfoVLAN = "0" + $DVPGInfoVLAN }
		if ([string]$DVPGInfoVLAN.length -eq 4) {$DVPGInfoVLAN = $DVPGInfoVLAN }
		$DVPGPattern = $DVPGInfoVLAN + "-" + $DatacenterView.name + "-" + $ClusterName
		if ($DVPGInfo.name -like "$DVPGPattern*"){
			write-host $DVPGInfo.Name " name is according to NNIT Standard " -foreground green
		} else {
			
			#add the descripton if it has it
			if ($DVPGInfoDescription[3]){
				$DVPGCorrectName = $DVPGPattern + "-" + $DVPGInfoDescription[3]
			} else {
				$DVPGCorrectName = $DVPGPattern
			}
			write-host $DVPGInfo.Name " should be " $DVPGCorrectName -foreground red
			write-output "Get-VDPortgroup -Name '$($DVPGInfo.name)' | Set-VDPortgroup -Name '$($DVPGCorrectName)'" >> $outfile
		}
		
	}
	
}

export-modulemember -function 'get-*'
export-modulemember -function 'new-*'
export-modulemember -function 'check-*'